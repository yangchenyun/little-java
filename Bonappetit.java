abstract class Seasoning {}
class Salt extends Seasoning {}
class Pepper extends Seasoning {}
class Thyme extends Seasoning {}

abstract class Point {
    int x;
    int y;

    abstract int distanceToO();
    boolean closerToO(Point other) {
        return distanceToO() <= other.distanceToO();
    }
    Point (int _x, int _y) {
        x = _x;
        y = _y;
    }
    Point minus(Point p) {
        return new CartesianPt(x - p.x, y - p.y);
    }
}

class CartesianPt extends Point {
    CartesianPt (int _x, int _y) {
        super(_x, _y);
    }

    int distanceToO() {
        return (int)Math.sqrt(x * x + y * y);
    }
}

class ManhattanPt extends Point {
    ManhattanPt (int _x, int _y) {
        super(_x, _y);
    }

    int distanceToO() {
        return x + y;
    }
}

class ShadowedManhattanPt extends ManhattanPt {
    int dX;
    int dY;
    ShadowedManhattanPt(int _x, int _y, int _dX, int _dY) {
        super(_x, _y);
        dX = _dX;
        dY = _dY;
    }
    int distanceToO() {
        return super.distanceToO() + dX + dY;
    }
}

class ShadowedCartesianPt extends CartesianPt {
    int dX;
    int dY;
    ShadowedCartesianPt(int _x, int _y, int _dX, int _dY) {
        super(_x, _y);
        dX = _dX;
        dY = _dY;
    }
    int distanceToO() {
        return new CartesianPt(x + dX, y + dY).distanceToO();
    }
}

abstract class Num {
    int val;

    int val() {
        return val;
    }

    @Override
    public String toString() {
        return super.toString() + ": " + val();
    }
}
class Zero extends Num {
    Zero () {
        val = 0;
    }

    public boolean equals(Object o) {
        return o instanceof Zero;
    }
}
class OneMoreThan extends Num {
    Num predecessor;

    OneMoreThan(Num _p) {
        predecessor = _p;
    }

    public boolean equals(Object o) {
        return o instanceof OneMoreThan &&
            predecessor.equals(((OneMoreThan) o).predecessor);
    }

    @Override
    public int val() {
        return 1 + predecessor.val();
    }
}

abstract class Layer {}
class Base extends Layer {
    Object o;
    Base(Object _o) {
        o = _o;
    }
}
class Slice extends Layer {
    Layer l;
    Slice(Layer _l) {
        l = _l;
    }
}

abstract class Shish {
    abstract boolean onlyOnions();
    abstract boolean isVegetarian();
    OnlyOnions ooFn = new OnlyOnions();
    IsVegetarian ivFn = new IsVegetarian();
}
class Skewer extends Shish {
    boolean onlyOnions() {
        return ooFn.forSkewer();
    };
    boolean isVegetarian() {
        return ivFn.forSkewer();
    }
}
class Onion extends Shish {
    Shish s;
    Onion(Shish _s) {
        s = _s;
    }

    boolean onlyOnions() {
        return ooFn.forOnion(s);
    };

    boolean isVegetarian() {
        return ivFn.forOnion(s);
    }
}

class Lamb extends Shish {
    Shish s;
    Lamb(Shish _s) {
        s = _s;
    }

    boolean onlyOnions() {
        return ooFn.forLamb(s);
    };

    boolean isVegetarian() {
        return ivFn.forLamb(s);
    }
}


class Tomato extends Shish {
    Shish s;
    Tomato(Shish _s) {
        s = _s;
    }
    boolean onlyOnions() {
        return ooFn.forTomato(s);
    };

    boolean isVegetarian() {
        return ivFn.forTomato(s);
    }
}

class OnlyOnions {
    boolean forSkewer() {
        return true;
    }
    boolean forOnion(Shish s) {
        return s.onlyOnions();
    }
    boolean forLamb(Shish s) {
        return false;
    }
    boolean forTomato(Shish s) {
        return false;
    }
}

class IsVegetarian {
    boolean forSkewer() {
        return true;
    }
    boolean forOnion(Shish s) {
        return s.isVegetarian();
    }
    boolean forLamb(Shish s) {
        return false;
    }
    boolean forTomato(Shish s) {
        return s.isVegetarian();
    }
}

// Kebab
abstract class Kebab {
    abstract boolean isVeggies();
    abstract Object whatHolder();
}
class Holder extends Kebab {
    Object o;
    Holder(Object _o) { // could place on any Object instead of only Kebab
        o = _o;
    }
    boolean isVeggies() {
        return true;
    }
    Object whatHolder() {
        return o;
    }
}
class Shallot extends Kebab {
    Kebab k;
    Shallot(Kebab _k) {
        k = _k;
    }
    boolean isVeggies() {
        return k.isVeggies();
    }
    Object whatHolder() {
        return k.whatHolder();
    }
}
class Shrimp extends Kebab {
    Kebab k;
    Shrimp(Kebab _k) {
        k = _k;
    }
    boolean isVeggies() {
        return false;
    }
    Object whatHolder() {
        return k.whatHolder();
    }
}
class Radish extends Kebab {
    Kebab k;
    Radish(Kebab _k) {
        k = _k;
    }
    boolean isVeggies() {
        return k.isVeggies();
    }
    Object whatHolder() {
        return k.whatHolder();
    }
}
class Zucchini extends Kebab {
    Kebab k;
    Zucchini(Kebab _k) {
        k = _k;
    }
    boolean isVeggies() {
        return k.isVeggies();
    }
    Object whatHolder() {
        return k.whatHolder();
    }
}

abstract class Rod {}
class Dagger extends Rod {}
class Sabre extends Rod {}
class Sword extends Rod {}

abstract class Plate {}
class Gold extends Plate {}
class Silver extends Plate {}
class Brass extends Plate {}
class Copper extends Plate {}
class Wood extends Plate {}

abstract class Pizza {
    Pizza p;
    Pizza(Pizza _p) {
        p = _p;
    }
    Pizza() {};
    abstract Pizza removeAnchovy();
    abstract Pizza topAnchovyWithCheese();
    abstract Pizza subAnchovyWithCheese();
    RemoveAnchovy raFn = new RemoveAnchovy();
    TopAnchovyWithCheese taFn = new TopAnchovyWithCheese();
    SubAnchovyWithCheese saFn = new SubAnchovyWithCheese();
}
class Crust extends Pizza {
    Pizza removeAnchovy() {
        return raFn.forCrust();
    }
    Pizza topAnchovyWithCheese() {
        return taFn.forCrust();
    }
    Pizza subAnchovyWithCheese() {
        return saFn.forCrust();
    }

    @Override
    public String toString() {
        return this.getClass().getName();
    }
}
class Cheese extends Pizza {
    Cheese(Pizza _p) {
        super(_p);
    }
    Pizza removeAnchovy() {
        return raFn.forCheese(p);
    }

    Pizza topAnchovyWithCheese() {
        return taFn.forCheese(p);
    }

    Pizza subAnchovyWithCheese() {
        return saFn.forCheese(p);
    }

    @Override
    public String toString() {
        return this.getClass().getName() + " > " + p;
    }
}
class Olive extends Pizza {
    Pizza p;
    Olive(Pizza _p) {
        p = _p;
    }
    Pizza removeAnchovy() {
        return raFn.forOlive(p);
    }
    Pizza topAnchovyWithCheese() {
        return taFn.forOlive(p);
    }

    Pizza subAnchovyWithCheese() {
        return saFn.forOlive(p);
    }

    @Override
    public String toString() {
        return this.getClass().getName() + " > " + p;
    }
}
class Anchovy extends Pizza {
    Pizza p;
    Anchovy(Pizza _p) {
        p = _p;
    }
    Pizza removeAnchovy() {
        return raFn.forAnchovy(p);
    }
    Pizza topAnchovyWithCheese() {
        return taFn.forAnchovy(p);
    }

    Pizza subAnchovyWithCheese() {
        return saFn.forAnchovy(p);
    }

    @Override
    public String toString() {
        return this.getClass().getName() + " > " + p;
    }
}
class Sausage extends Pizza {
    Pizza p;
    Sausage(Pizza _p) {
        p = _p;
    }
    Pizza removeAnchovy() {
        return raFn.forSausage(p);
    }
    Pizza topAnchovyWithCheese() {
        return taFn.forSausage(p);
    }

    Pizza subAnchovyWithCheese() {
        return saFn.forSausage(p);
    }

    @Override
    public String toString() {
        return this.getClass().getName() + " > " + p;
    }
}

class Spinach extends Pizza {
    Pizza p;
    Spinach(Pizza _p) {
        p = _p;
    }
    Pizza removeAnchovy() {
        return raFn.forSpinach(p);
    }
    Pizza topAnchovyWithCheese() {
        return taFn.forSpinach(p);
    }

    Pizza subAnchovyWithCheese() {
        return saFn.forSpinach(p);
    }

    @Override
    public String toString() {
        return this.getClass().getName() + " > " + p;
    }
}

class RemoveAnchovy {
    Pizza forCrust() {
        return new Crust();
    }

    Pizza forCheese(Pizza p) {
        return new Cheese(p.removeAnchovy());
    }

    Pizza forOlive(Pizza p) {
        return new Olive(p.removeAnchovy());
    }

    Pizza forAnchovy(Pizza p) {
        return p.removeAnchovy();
    }

    Pizza forSausage(Pizza p) {
        return new Sausage(p.removeAnchovy());
    }

    Pizza forSpinach(Pizza p) {
        return new Spinach(p.removeAnchovy());
    }
}

class TopAnchovyWithCheese {
    Pizza forCrust() {
        return new Crust();
    }

    Pizza forCheese(Pizza p) {
        return new Cheese(p.topAnchovyWithCheese());
    }

    Pizza forOlive(Pizza p) {
        return new Olive(p.topAnchovyWithCheese());
    }

    Pizza forAnchovy(Pizza p) {
        return new Cheese
            (new Anchovy
             (p.topAnchovyWithCheese()));
    }

    Pizza forSausage(Pizza p) {
        return new Sausage(p.topAnchovyWithCheese());
    }

    Pizza forSpinach(Pizza p) {
        return new Spinach(p.topAnchovyWithCheese());
    }
}

class SubAnchovyWithCheese {
    Pizza forCrust() {
        return new Crust();
    }

    Pizza forCheese(Pizza p) {
        return new Cheese(p.subAnchovyWithCheese());
    }

    Pizza forOlive(Pizza p) {
        return new Olive(p.subAnchovyWithCheese());
    }

    Pizza forAnchovy(Pizza p) {
        return new Cheese(p.subAnchovyWithCheese());
    }

    Pizza forSausage(Pizza p) {
        return new Sausage(p.subAnchovyWithCheese());
    }

    Pizza forSpinach(Pizza p) {
        return new Spinach(p.subAnchovyWithCheese());
    }
}

abstract class Fish {
    @Override
    public String toString() {
        return getClass().getName();
    }
}
class Salmon extends Fish {
    public boolean equals(Object o) {
        return (o instanceof Salmon);
    }
}
class Thryssa extends Fish {
    public boolean equals(Object o) {
        return (o instanceof Thryssa);
    }
}
class Tuna extends Fish {
    public boolean equals(Object o) {
        return (o instanceof Tuna);
    }
}

abstract class PizzaPie {
    abstract Object accept(PieVisit fn);
}
class Bottom extends PizzaPie {
    @Override
    public String toString() {
        return getClass().getName();
    }

    Object accept(PieVisit fn) {
        return fn.forBottom(this);
    }
}
class Topping extends PizzaPie {
    Object t;
    PizzaPie r;

    Topping(Object _t, PizzaPie _r) {
        t = _t;
        r = _r;
    }

    Object accept(PieVisit fn) {
        return fn.forTop(this);
    }
    @Override
    public String toString() {
        return getClass().getName() + ":" + t + " > " + r;
    }
}

interface PieVisit {
    Object forBottom(Bottom that);
    Object forTop(Topping that);
}
class Rem implements PieVisit {
    Object o;
    Rem(Object _o) {
        o = _o;
    }
    public PizzaPie forBottom(Bottom that) {
        return new Bottom();
    }
    public PizzaPie forTop(Topping that) {
        if (o.equals(that.t)) {
            return (PizzaPie)that.r.accept(this);
        } else {
            return new Topping(that.t, (PizzaPie)that.r.accept(this));
        }
    }
}

class Subst implements PieVisit {
    Object o;
    Object s;
    Subst(Object _o, Object _s) {
        o = _o;
        s = _s;
    }
    public PizzaPie forBottom(Bottom that) {
        return new Bottom();
    }
    public PizzaPie forTop(Topping that) {
        if (o.equals(that.t)) {
            that.t = s;
            that.r.accept(this);
            return that;
        } else {
            that.r.accept(this);
            return that;
        }
    }
}

class LtdSubst extends Subst {
    Integer l;
    LtdSubst(Integer _l, Object _o, Object _s) {
        super(_o, _s);
        l = _l;
    }
    public PizzaPie forTop(Topping that) {
        if (l == 0) {
            return that;
        }
        if (o.equals(that.t)) {
            that.t = s;
            that.r.accept(new LtdSubst(l - 1, o, s));
            return that;
        } else {
            that.r.accept(this);
            return that;
        }
    }
}

// Chapter 7
abstract class Fruit {
    @Override
    public String toString() {
        return this.getClass().getName();
    }
}
class Apple extends Fruit {
    public boolean equals(Object o) {
        return o instanceof Apple;
    }
}
class Peach extends Fruit {
    public boolean equals(Object o) {
        return o instanceof Peach;
    }
}
class Pear extends Fruit {
    public boolean equals(Object o) {
        return o instanceof Pear;
    }
}
class Fig extends Fruit {
    public boolean equals(Object o) {
        return o instanceof Fig;
    }
}

abstract class Tree {
    abstract Object accept(TreeVisitor visitor);
}
class Bud extends Tree {
    @Override
    public String toString() {
        return getClass().getName();
    }
    Object accept(TreeVisitor visitor) {
        return visitor.forBud();
    }
}
class Flat extends Tree {
    Fruit f;
    Tree t;
    Flat(Fruit _f, Tree _t) {
        f = _f;
        t = _t;
    }
    Object accept(TreeVisitor visitor) {
        return visitor.forFlat(f, t);
    }
    @Override
    public String toString() {
        return getClass().getName() + "(" + f + ") > " +  t;
    }
}
class Split extends Tree {
    Tree l;
    Tree r;
    Split(Tree _l, Tree _r) {
        l = _l;
        r = _r;
    }
    Object accept(TreeVisitor visitor) {
        return visitor.forSplit(l, r);
    }
}

interface TreeVisitor {
    Object forBud();
    Object forFlat(Fruit f, Tree t);
    Object forSplit(Tree l, Tree r);
}

class IsFlat implements TreeVisitor {
    public Object forBud() {
        return true;
    }
    public Object forFlat(Fruit f, Tree t) {
        return t.accept(this);
    }
    public Object forSplit(Tree l, Tree r) {
        return false;
    }
}

class IsSplit implements TreeVisitor {
    public Object forBud() {
        return true;
    }
    public Object forFlat(Fruit f, Tree t) {
        return false;
    }
    public Object forSplit(Tree l, Tree r) {
        return (Boolean) l.accept(this) && (Boolean) r.accept(this);
    }
}

class HasFruit implements TreeVisitor {
    public Object forBud() {
        return false;
    }
    public Object forFlat(Fruit f, Tree t) {
        return true;
    }
    public Object forSplit(Tree l, Tree r) {
        return (Boolean )l.accept(this) || (Boolean) r.accept(this);
    }
}

class Height implements TreeVisitor {
    public Object forBud() {
        return 0;
    }
    public Object forFlat(Fruit f, Tree t) {
        return (int) t.accept(this) + 1;
    }
    public Object forSplit(Tree l, Tree r) {
        return 1 + Math.max((int)l.accept(this), (int)r.accept(this));
    }
}

class FruitOccur implements TreeVisitor {
    Fruit o;
    FruitOccur(Fruit _o) {
        o = _o;
    }
    public Object forBud() {
        return 0;
    }
    public Object forFlat(Fruit f, Tree t) {
        return (int)t.accept(this) + (o.equals(f) ? 1 : 0);
    }
    public Object forSplit(Tree l, Tree r) {
        return (int)l.accept(this) + (int)r.accept(this);
    }
}
class FruitSubst implements TreeVisitor {
    Fruit u;
    Fruit o;

    FruitSubst(Fruit _u, Fruit _o) {
        u = _u;
        o = _o;
    }

    public Object forBud() {
        return new Bud();
    }
    public Object forFlat(Fruit f, Tree t) {
        if (f.equals(u)) {
            return new Flat(o, (Tree)t.accept(this));
        } else {
            return new Flat(f, (Tree)t.accept(this));
        }
    }
    public Object forSplit(Tree l, Tree r) {
        return new Split((Tree)l.accept(this), (Tree)r.accept(this));
    }
}

// Expression
abstract class Expr {
    abstract Object accept(ExprVisitor visit);
}
class Plus extends Expr {
    Expr l;
    Expr r;
    Plus(Expr _l, Expr _r) {
        l = _l;
        r = _r;
    }
    Object accept(ExprVisitor visit) {
        return visit.forPlus(l, r);
    }

}

class Diff extends Expr {
    Expr l;
    Expr r;
    Diff(Expr _l, Expr _r) {
        l = _l;
        r = _r;
    }
    Object accept(ExprVisitor visit) {
        return visit.forDiff(l, r);
    }
}

class Prod extends Expr {
    Expr l;
    Expr r;
    Prod(Expr _l, Expr _r) {
        l = _l;
        r = _r;
    }
    Object accept(ExprVisitor visit) {
        return visit.forProd(l, r);
    }
}
class Const extends Expr {
    Object c;
    Const(Object _c) {
        c = _c;
    }
    Object accept(ExprVisitor visit) {
        return visit.forConst(c);
    }
}

interface ExprVisitor {
    Object forPlus(Expr l, Expr r);
    Object forDiff(Expr l, Expr r);
    Object forProd(Expr l, Expr r);
    Object forConst(Object o);
}

abstract class Eval implements ExprVisitor {
    public Object forPlus(Expr l, Expr r) {
        return plus(l.accept(this), r.accept(this));
    }
    public Object forDiff(Expr l, Expr r) {
        return diff(l.accept(this), r.accept(this));
    }
    public Object forProd(Expr l, Expr r) {
        return prod(l.accept(this), r.accept(this));
    }
    public Object forConst(Object o) {
        return o;
    }
    abstract Object plus(Object l, Object r);
    abstract Object diff(Object l, Object r);
    abstract Object prod(Object l, Object r);
}

class IntegerEval extends Eval {
    // separate the recursion part from the actual manipulation part
    Object plus(Object l, Object r) {
        return new Integer
            (((Integer)l).intValue()
             +
             ((Integer)r).intValue());
    }

    Object diff(Object l, Object r) {
        return new Integer
            (((Integer)l).intValue()
             -
             ((Integer)r).intValue());
    }

    Object prod(Object l, Object r) {
        return new Integer
            (((Integer)l).intValue()
             *
             ((Integer)r).intValue());
    }
}

abstract class Set {
    Set add(Integer i) {
        if (mem(i)) {
            return this;
        } else {
            return new Add(i, this);
        }
    }

    abstract boolean mem(Integer i);
    abstract Set diff(Set o);
    abstract Set plus(Set o);
    abstract Set prod(Set o);
}

class Empty extends Set {
    boolean mem(Integer i) {
        return false;
    }
    Set diff(Set o) {
        return new Empty();
    }
    Set plus(Set o) {
        return o;
    }
    Set prod(Set o) {
        return new Empty();
    }

    @Override
    public String toString() {
        return "";
    }
}
class Add extends Set {
    Set s;
    Integer m; // the incremental int

    Add(Integer _m, Set _s) {
        m = _m;
        s = _s;
    }

    boolean mem(Integer i) {
        return m.equals(i) ? true : s.mem(i);
    }

    Set diff(Set o) {
        if (o.mem(m)) {
            return s.diff(o);
        } else {
            return s.diff(o).add(m);
        }
    }
    Set plus(Set o) {
        return s.plus(o.add(m));
    }
    Set prod(Set o) {
        if (o.mem(m)) {
            return s.prod(o).add(m);
        } else {
            return s.prod(o);
        }
    }

    @Override
    public String toString() {
        return String.valueOf(m) + " " + s;
    }
}

class SetEval extends Eval {
    Object plus(Object l, Object r) {
        return ((Set)l).plus((Set) r);
    }

    Object diff(Object l, Object r) {
        return ((Set)l).diff((Set) r);
    }

    Object prod(Object l, Object r) {
        return ((Set)l).prod((Set) r);
    }
}

interface ShapeVisitor {
    Object forCircle(int r);
    Object forSquare(int s);
    Object forTrans(Point p, Shape s);
}
abstract class Shape {
    abstract Object accept(ShapeVisitor visit);
}
class Circle extends Shape {
    int r;
    Circle(int _r) {
        r = _r;
    }
    Object accept(ShapeVisitor visit) {
        return visit.forCircle(r);
    }
}

class Square extends Shape {
    int s;
    Square(int _s) {
        s = _s;
    }
    Object accept(ShapeVisitor visit) {
        return visit.forSquare(s);
    }
}

class Trans extends Shape {
    Point p;
    Shape s;
    Trans(Point _p, Shape _s) {
        p = _p;
        s = _s;
    }
    Object accept(ShapeVisitor visit) {
        return visit.forTrans(p, s);
    }
}

interface UnionVisitor extends ShapeVisitor {
    Object forUnion(Shape s, Shape t);
}
class Union extends Shape {
    Shape s;
    Shape t;
    Union(Shape _s, Shape _t) {
        s = _s;
        t = _t;
    }
    Object accept(ShapeVisitor visit) {
        return ((UnionVisitor)visit).forUnion(s, t);
    }
}

class HasPt implements ShapeVisitor {
    Point q;
    HasPt(Point _q) {
        q = _q;
    }
    ShapeVisitor newHasPt(Point p) {
        return new HasPt(p);
    }
    public Object forCircle(int r) {
        return q.distanceToO() <= r;
    }
    public Object forSquare(int s) {
        return q.x <= s && q.y <= s;
    }
    public Object forTrans(Point p, Shape s) {
        return s.accept(newHasPt((q.minus(p))));
    }
}

class UnionHasPt
    extends HasPt
    implements UnionVisitor {
    UnionHasPt(Point _q) {
        super(_q);
    }
    @Override
    ShapeVisitor newHasPt(Point p) {
        return new UnionHasPt(p);
    }
    public Object forUnion(Shape s, Shape t) {
        return (Boolean)s.accept(this) || (Boolean)t.accept(this);
    }
}

interface iPieman {
    int addTop(Object t);
    int remTop(Object t);
    int substTop(Object n, Object o);
    int occTop(Object o);
}

class Occurs implements PieVisit {
    Object o;
    Occurs(Object _o) {
        o = _o;
    }
    public Object forBottom(Bottom that) {
        return new Integer(0);
    }
    public Object forTop(Topping that) {
        if (that.t.equals(o)) {
            return new Integer
                (((Integer)that.r.accept(this)).intValue() + 1);
        } else {
            return that.r.accept(this);
        }
    }
}

class Pieman implements iPieman {
    PizzaPie p = new Bottom();
    public int addTop(Object t) {
        p = new Topping(t, p);
        return occTop(t);
    }
    public int remTop(Object t) {
        p = (PizzaPie)p.accept(new Rem(t));
        return occTop(t);
    }
    public int substTop(Object o, Object s) {
        p = (PizzaPie)p.accept(new Subst(o, s));
        return occTop(s);
    }

    public int occTop(Object o) {
        return ((Integer)p.accept(new Occurs(o))).intValue();
    }
}

class Bonappetit {
    public static void main(String[] args) {
        Salt salt = new Salt();
        Pepper pepper = new Pepper();
        System.out.println(salt);
        System.out.println(pepper);

        ManhattanPt pt = new ManhattanPt(10, 12);
        System.out.println(pt);

        // Num
        Num zero = new Zero();
        System.out.println(zero);

        Num one = new OneMoreThan(zero);
        System.out.println(one);

        Num two = new OneMoreThan(one);
        System.out.println(two);
        System.out.println(new OneMoreThan(two));
        System.out.println(new OneMoreThan(new OneMoreThan (two)));

        // Layer
        // declare primitive explicit as Objects
        System.out.println(new Base(new Integer(5)));
        System.out.println(new Base(new Boolean(true)));

        // Back to Point
        ManhattanPt mpt = new ManhattanPt(10, 12);
        System.out.println(mpt.distanceToO());

        CartesianPt cpt = new CartesianPt(9, 12);
        System.out.println(cpt.distanceToO());

        // Shish
        System.out.println(new Skewer());
        System.out.println(new Onion
                           (new Skewer()));
        System.out.println(new Lamb
                           (new Onion
                            (new Skewer())));
        System.out.println(new Lamb
                           (new Onion
                            (new Skewer())).onlyOnions());
        System.out.println(new Onion
                           (new Onion
                            (new Skewer())).onlyOnions());
        System.out.println(new Onion
                           (new Lamb
                            (new Skewer())).onlyOnions());

        System.out.println(new Tomato
                           (new Onion
                            (new Lamb
                             (new Skewer()))).isVegetarian());

        System.out.println(new Tomato
                           (new Onion
                            (new Tomato
                             (new Skewer()))).isVegetarian());

        // Kebab
        System.out.println(new Shallot
                           (new Radish
                            (new Holder
                             (new Dagger()))).isVeggies());

        System.out.println(new Shallot
                           (new Shrimp
                            (new Holder
                             (new Gold()))).isVeggies());

        System.out.println(new Shallot
                           (new Shrimp
                            (new Holder
                             (new Dagger()))).whatHolder().getClass().getName());

        System.out.println(new Shallot
                           (new Shrimp
                            (new Holder
                             (new Integer(42)))).whatHolder().getClass().getName());

        System.out.println(new Zucchini
                           (new Shallot
                            (new Shrimp
                             (new Holder
                              (new Dagger())))));

        // back to Point
        System.out.println(mpt.closerToO(new ManhattanPt(3, 4)));
        System.out.println(cpt.closerToO(new CartesianPt(10, 40)));
        System.out.println(mpt.closerToO(new CartesianPt(10, 40)));

        System.out.println(new Anchovy
                           (new Olive
                            (new Anchovy
                             (new Anchovy
                              (new Cheese
                               (new Crust()))))).removeAnchovy());

        System.out.println(new Anchovy
                           (new Olive
                            (new Anchovy
                             (new Anchovy
                              (new Cheese
                               (new Crust()))))).topAnchovyWithCheese());

        System.out.println(new Anchovy
                           (new Olive
                            (new Anchovy
                             (new Anchovy
                              (new Spinach
                               (new Crust()))))).subAnchovyWithCheese());

        // PizzaPie
        System.out.println(new Topping
                           (new Tuna(),
                            (new Topping
                             (new Salmon(),
                              new Topping
                              (new Thryssa(),
                               new Bottom())))));

        System.out.println(new Topping
                           (new Thryssa(),
                            (new Topping
                             (new Salmon(),
                              new Topping
                              (new Tuna(),
                               new Bottom())))).accept(new Rem(new Thryssa())));

        System.out.println(new Topping
                           (new Thryssa(),
                            (new Topping
                             (new Tuna(),
                              new Topping
                              (new Tuna(),
                               new Bottom())))).accept(new Rem(new Tuna())));

        System.out.println(new Topping
                           (new Integer(3),
                            (new Topping
                             (new Integer(5),
                              new Topping
                              (new Integer(2),
                               new Bottom())))).accept(new Rem(new Integer(2))));

        System.out.println(new Topping
                           (new Integer(3),
                            (new Topping
                             (new Zero(),
                              new Topping
                              (new Integer(2),
                               new Bottom())))).accept(new Rem(new Zero())));

        System.out.println(new Topping
                           (new Integer(3),
                            (new Topping
                             (new OneMoreThan(new Zero()),
                              new Topping
                              (new Integer(2),
                               new Bottom())))).accept(new Rem(new OneMoreThan(new Zero()))));

        System.out.println(new Topping
                           (new Integer(3),
                            (new Topping
                             (new OneMoreThan(new Zero()),
                              new Topping
                              (new Integer(2),
                               new Bottom())))).accept(new Subst
                                                      (new OneMoreThan(new Zero()),
                                                       new Tuna())));

        System.out.println(new Topping
                           (new Tuna(),
                            (new Topping
                             (new Salmon(),
                              new Topping
                              (new Tuna(),
                               new Topping
                               (new Tuna(),
                                new Topping
                                (new Salmon(),
                                 (new Bottom()))))))).accept(new LtdSubst
                                                             (2,
                                                              new Tuna(),
                                                              new Thryssa())));

        System.out.println(new Flat
                           (new Apple(),
                            new Flat
                            (new Peach(),
                             new Flat
                             (new Pear(),
                              new Bud()))).accept(new IsFlat()));

        System.out.println(new Split
                           (new Bud(),
                            new Split
                            (new Bud(),
                             new Split
                             (new Bud(),
                              new Bud()))).accept(new IsSplit()));

        System.out.println(new Split
                           (new Bud(),
                            new Split
                            (new Bud(),
                             new Split
                             (new Bud(),
                              new Bud()))).accept(new HasFruit()));

        System.out.println(new Split
                           (new Bud(),
                            new Flat
                            (new Peach(),
                             new Split
                             (new Bud(),
                              new Bud()))).accept(new HasFruit()));

        System.out.println(new Split
                             (new Bud(),
                              new Bud()).accept(new Height()));

        System.out.println(new Split
                           (new Bud(),
                            new Flat
                            (new Peach(),
                             new Split
                             (new Bud(),
                              new Bud()))).accept(new Height()));

        System.out.println(new Split
                           (new Bud(),
                            new Flat
                            (new Apple(),
                             new Split
                             (new Flat
                              (new Apple(),
                               new Bud()),
                              new Bud()))).accept(new FruitOccur
                                                  (new Apple())));

        System.out.println(((Tree)
                            (new Split
                             (new Bud(),
                              new Flat
                              (new Apple(),
                               new Split
                               (new Flat
                                (new Apple(),
                                 new Bud()),
                                new Bud())))
                             .accept(new FruitSubst
                                     (new Apple(),
                                      new Fig()))))
                           .accept(new FruitOccur(new Fig())));

        System.out.println(new Plus(
                                    new Const(new Integer(7)),
                                    new Prod(
                                             new Diff(
                                                      new Const(new Integer(10)),
                                                      new Const(new Integer(3))),
                                             new Const(new Integer(5)))).accept(new IntegerEval()));

        System.out.println(new Plus(
                                    new Const(
                                              new Empty()
                                              .add(new Integer(7))
                                              .add(new Integer(5))),
                                    new Prod(
                                             new Diff(
                                                      new Const(new Empty()
                                                                .add(new Integer(4))
                                                                .add(new Integer(3))),
                                                      new Const(new Empty().add(new Integer(3)))),
                                             new Const(new Empty()
                                                       .add(new Integer(5))
                                                       .add(new Integer(4))))).accept(new SetEval()));


        System.out.println(new ShadowedCartesianPt(12, 5, 3, 4).distanceToO());
        System.out.println(new ShadowedCartesianPt(1, 5, 1, 2).closerToO
                           (new CartesianPt(3, 4)));

        System.out.println(new Circle(10).accept
                           (new HasPt(new CartesianPt(10, 10))));

        System.out.println(new Square(10).accept
                           (new HasPt(new CartesianPt(10, 10))));

        System.out.println(new Circle(10).accept
                           (new HasPt(new CartesianPt(5, 5))));

        System.out.println(new Trans
                           (new CartesianPt(5, 6),
                            new Circle(10)).accept
                           (new HasPt(new CartesianPt(5, 5))));

        System.out.println(new Trans
                           (new CartesianPt(3, 7),
                            new Union
                            (new Square(10),
                             new Circle(10))).accept
                           (new UnionHasPt(new CartesianPt(13, 17))));

        System.out.println(new Pieman().occTop(new Tuna()));

        Pieman pm = new Pieman();
        pm.addTop(new Tuna());
        pm.substTop(new Tuna(), new Thryssa());
        System.out.println(pm.occTop(new Thryssa()));

        Pieman yy = new Pieman();
        yy.addTop(new Tuna());
        yy.addTop(new Tuna());
        yy.addTop(new Salmon());
        yy.addTop(new Salmon());
        yy.addTop(new Thryssa());
        System.out.println(yy.substTop(new Tuna(), new Thryssa()));
        System.out.println(yy.remTop(new Tuna()));
    }
}
